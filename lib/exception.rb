module GameErrors
  class RobotDeadError < StandardError
  end

  class NotaRobotError < StandardError
  end

end