
require_relative 'item'
require 'pry'
require_relative 'exception'

class Robot
include GameErrors

MAX_CAPACITY = 250 

attr_reader :position,:items,:items_weight,:health,:equipped_weapon
attr_writer :equipped_weapon

def initialize
  @items = []
  @position =[ 0,0]
  @items_weight = 0
  @health = 100
  @equipped_weapon = nil 
end

def move_left
  @position[0] -= 1
end

def move_right
  @position[0] += 1
end

def move_up
  @position[1] += 1
end

def move_down
  @position[1] -= 1
end

def pick_up(item)
  unless item.weight + items_weight > MAX_CAPACITY
  @equipped_weapon = item if item.is_a? Weapon
  @items << item
end
end

def items_weight
  @items_weight = @items.inject(0){|sum, item| sum + item.weight}
end

def wound(damagevalue)
 (@health -= damagevalue) < 0 ? @health = 0 : @health
end

def heal(healvalue)
  (@health += healvalue) > 100 ? @health = 100 : @health
end

def attack(enemy)
  if equipped_weapon
    equipped_weapon.hit(enemy)
  else
  enemy.wound(5)
  end
end

def heal!
  begin 
    if @health == 0 
      raise RobotDeadError , 'The robot is already dead'
    else
      heal(healvalue)
    end
    rescue RobotDeadError => e
  end
end

def attack!(enemy)
  begin 
    if enemy.is_a? Item
      raise NotaRobotError , 'Can only attack a Robot!'
    else
      attack(enemy)
    end
  rescue NotaRobotError => e
  end
end


end
