require_relative 'spec_helper'

describe Robot do
  before :each do
    @robot = Robot.new
    @bolt = BoxOfBolts.new
  end

  describe "#heal!" do 
    it "should raise an error" do 
      @robot.wound(150)
      @robot.heal!
      # expect { raise RobotDeadError }.to raise_error
       expect { raise RobotDeadError, 'The robot is already dead'}
    end
end

    describe "#attack!" do 
    it "should raise an error" do 
      @robot.attack!(@bolt)
       expect { raise NotaRobotError, 'Can only attack a Robot!'}
    end
end

end


